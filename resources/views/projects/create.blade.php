@extends('app')

@section('content')
    <h2>Create Project</h2>

    {!! Form::model(new App\Project, ['route' => 'projects.store']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name') !!}
        </div>
        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug') !!}
        </div>
        <div class="form-group">
            {!! Form::hidden('user_id', auth()->user()->id) !!}
        </div>
        {!! Form::submit('Create', array('class'=>'btn btn-primary')) !!}
    {!! Form::close() !!}

@endsection