@extends('app')

@section('content')
    <h2>Edit Project</h2>

    {!! Form::model($project, ['method' => 'PATCH', 'route' => ['projects.update', $project->slug]]) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name') !!}
    </div>
    <div class="form-group">
        {!! Form::label('slug', 'Slug:') !!}
        {!! Form::text('slug') !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Edit Project', ['class'=>'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@endsection