@extends('app')

@section('content')
    <h2>Projects</h2>

    @if(!auth()->guest())

        <ul>
            @foreach($projects as $p)
                @if($p->user_id == auth()->user()->id)
                <li style="margin-top: 10px;">
                    {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('projects.destroy', $p->slug))) !!}
                    <a href="{{ route('projects.show', $p->slug) }}">{{$p->name}}</a>
                    (
                    {!! link_to_route('projects.edit', 'Edit', array($p->slug), array('class' => 'btn btn-info')) !!}
                    {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                    ) - By {{ $p->user->name }}
                    {!! Form::close() !!}
                </li>
                @endif
            @endforeach
        </ul>

    @else
        @if( !$projects->count() )
            No projects
        @else
            <ul>
                @foreach($projects as $p)
                    <li style="margin-top: 10px;">
                        <a href="{{ route('projects.show', $p->slug) }}">{{$p->name}}</a> - By {{ $p->user->name }}
                    </li>
                @endforeach
            </ul>
        @endif
    @endif

    <p>
        {!! link_to_route('projects.create', 'Create Project', array('class'=>'btn btn-info')) !!}
    </p>
@endsection