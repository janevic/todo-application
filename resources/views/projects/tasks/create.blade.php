@extends('app')

@section('content')
    <h2>Create Task for Project "{{ $project->name }}"</h2>

    {!! Form::model(new App\Task, ['route' => ['projects.tasks.store', $project->slug], 'class'=>'']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Name:') !!}
            {!! Form::text('name') !!}
        </div>

        <div class="form-group">
            {!! Form::label('slug', 'Slug:') !!}
            {!! Form::text('slug') !!}
        </div>

        <div class="form-group">
            {!! Form::label('completed', 'Completed:') !!}
            {!! Form::checkbox('completed') !!}
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description') !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Create Task', array('class'=>'btn btn-primary')) !!}
        </div>
    {!! Form::close() !!}
@endsection