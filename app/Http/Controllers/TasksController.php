<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TasksController extends Controller
{
    //
    /*
     * Validation part
     * */
    protected $rules = [
        "name" => ["required", "min:3"],
        "slug" => ["required"],
        "description" => ["required"],
    ];

    /*public function index(Project $project) {
        return view('projects.tasks.index', compact('project'));
    }*/

    public function store(Request $request, Project $project) {
        $this->validate($request, $this->rules);

        $input = Input::all();
        $input['project_id'] = $project->id;
        Task::create($input);

        return Redirect::route('projects.show', $project->slug)->with('message', 'Task created');
    }

    public function  create(Project $project) {
        return view('projects.tasks.create', compact('project'));
    }

    public function update(Request $request, Project $project, Task $task) {

        $this->validate($request, $this->rules);

        $input = array_except(Input::all(), '_method');
        $task->update($input);

        return Redirect::route('projects.show', [$project->slug])->with('message', 'Task updated');
    }

    public function destroy(Project $project, Task $task) {

        $task->delete();

        return Redirect::route('projects.show', $project->slug)->with('message', 'Task deleted');
    }

    public function show(Project $project, Task $task) {
        return view('projects.tasks.show', compact('project', 'task'));
    }

    public function edit(Project $project, Task $task) {
        return view('projects.tasks.edit', compact('project', 'task'));
    }
}
