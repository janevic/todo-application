<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::model('tasks', 'Task');
Route::model('projects', 'Project');

Route::bind('tasks', function($value, $route){
    return App\Task::whereSlug($value)->first();
});

Route::bind('projects', function($value, $route){
    return App\Project::whereSlug($value)->first();
});

Route::resource('projects', 'ProjectsController');
Route::resource('projects.tasks', 'TasksController');

Route::get('/', function(){
    return redirect('projects');
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('projects/create', ['as' => 'projects.create', 'uses'=> 'ProjectsController@create']);
    Route::get('projects/{projects}/tasks/create', ['as' => 'projects.tasks.create', 'uses' => 'TasksController@create']);
});

Route::group(['middleware' => 'web'], function(){
    Route::auth();
});