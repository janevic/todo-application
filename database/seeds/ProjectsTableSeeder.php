<?php

/**
 * Created by PhpStorm.
 * User: Darko
 * Date: 04-Jul-16
 * Time: 6:43 PM
 */
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{

    public function run() {
        DB::table('projects')->delete();

        $projects = array(
            ['id' => 1, 'name' => 'Project 1', 'slug' => 'project-1', 'user_id' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 2, 'name' => 'Project 2', 'slug' => 'project-2', 'user_id' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['id' => 3, 'name' => 'Project 3', 'slug' => 'project-3', 'user_id' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        DB::table('projects')->insert($projects);
    }
}