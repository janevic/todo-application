<?php

/**
 * Created by PhpStorm.
 * User: Darko
 * Date: 09-Jul-16
 * Time: 10:16 PM
 */
class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        $users = array(
            ['id' => 1, 'name' => 'darko', 'email' => 'darko.janevic@gmail.com', 'password' => "32153215", 'remember_token' => '', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        //// Uncomment the below to run the seeder
        DB::table('users')->insert($users);
    }
}