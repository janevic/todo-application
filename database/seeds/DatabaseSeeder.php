<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        \Illuminate\Database\Eloquent\Model::unguard();

        $this->call('UsersTableSeeder');
        $this->call('ProjectsTableSeeder');
        $this->call('TasksTableSeeder');

        \Illuminate\Database\Eloquent\Model::reguard();
    }
}

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        $users = array(
            ['id' => 1, 'name' => 'darko', 'email' => 'darko.janevic@gmail.com', 'password' => "32153215", 'remember_token' => '', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        );

        //// Uncomment the below to run the seeder
        DB::table('users')->insert($users);
    }
}